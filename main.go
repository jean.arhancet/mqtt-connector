package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type WattSenseReceive struct {
	PropertyId  string `json:"propertyId"`
	RequestType string `json:"requestType"`
	Value       string `json:"value"`
}

type WattSensePublish struct {
	PropertyId string    `json:"propertyId"`
	Timestamp  time.Time `json:"timestamp"`
	Value      int       `json:"value"`
}

const (
	broker          = "tcps://35.240.113.102:8883"
	clientID        = "bds-env-testing-device"
	topic_subscribe = "mqtt/bds-env-testing/setpoints"
	topic_publish   = "mqtt/bds-env-testing/collector"
	qos             = 0
)

func NewTLSConfig() *tls.Config {
	// Import trusted certificates from CAfile.pem.
	// Alternatively, manually add CA certificates to
	// default openssl CA bundle.
	certpool := x509.NewCertPool()
	pemCerts, err := ioutil.ReadFile("certs/CAfile.pem")
	if err == nil {
		certpool.AppendCertsFromPEM(pemCerts)
	}

	// Import client certificate/key pair
	cert, err := tls.LoadX509KeyPair("certs/client-crt.pem", "certs/client-key.pem")
	if err != nil {
		log.Panic(err)
	}

	// Just to print out the client certificate..
	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		log.Panic(err)
	}

	// Create tls.Config with desired tls properties
	return &tls.Config{
		// RootCAs = certs used to verify server cert.
		RootCAs: certpool,
		// ClientAuth = whether to request cert from server.
		// Since the server is set up for SSL, this happens
		// anyways.
		ClientAuth: tls.NoClientCert,
		// ClientCAs = certs used to validate client cert.
		ClientCAs: nil,
		// InsecureSkipVerify = verify that cert contents
		// match server. IP matches what is in cert etc.
		InsecureSkipVerify: true,
		// Certificates = list of certs client sends to server.
		Certificates: []tls.Certificate{cert},
	}
}

func main() {
	// Logging is off by default
	mqtt.ERROR = log.New(os.Stdout, "[ERROR] ", log.Ltime)
	mqtt.CRITICAL = log.New(os.Stdout, "[CRIT] ", log.Ltime)
	mqtt.WARN = log.New(os.Stdout, "[WARN]  ", log.Ltime)
	mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", log.Ltime)

	tlsconfig := NewTLSConfig()

	var wattSenseReceive WattSenseReceive

	log.Println("Start Mqtt connector")

	opts := mqtt.NewClientOptions()
	opts.AddBroker(broker)
	opts.SetClientID(clientID).SetTLSConfig(tlsconfig)
	opts.SetCleanSession(false)
	opts.SetAutoReconnect(true)

	opts.SetOrderMatters(false)       // Allow out of order messages (use this option unless in order delivery is essential)
	opts.ConnectTimeout = time.Second // Minimal delays on connect
	opts.WriteTimeout = time.Second   // Minimal delays on writes
	opts.KeepAlive = 10               // Keepalive every 10 seconds so we quickly detect network outages
	opts.PingTimeout = time.Second    // local broker so response should be quick

	// Automate connection management (will keep trying to connect and will reconnect if network drops)
	opts.ConnectRetry = true
	opts.AutoReconnect = true

	// If using QOS2 and CleanSession = FALSE then it is possible that we will receive messages on topics that we
	// have not subscribed to here (if they were previously subscribed to they are part of the session and survive
	// disconnect/reconnect). Adding a DefaultPublishHandler lets us detect this.
	opts.DefaultPublishHandler = func(_ mqtt.Client, msg mqtt.Message) {
		log.Printf("UNEXPECTED MESSAGE: %s\n", msg)
	}

	// Log events
	opts.OnConnectionLost = func(cl mqtt.Client, err error) {
		log.Println("connection lost")
	}

	opts.OnConnect = func(c mqtt.Client) {
		log.Println("connection established")

		// Establish the subscription - doing this here means that it will happen every time a connection is established
		// (useful if opts.CleanSession is TRUE or the broker does not reliably store session data)
		t := c.Subscribe(topic_subscribe, qos, func(c mqtt.Client, msg mqtt.Message) {

			// Establish the subscription - doing this here means that it will happen every time a connection is established
			// (useful if opts.CleanSession is TRUE or the broker does not reliably store session data)
			msgReceive := msg.Payload()
			log.Printf("Receive message %s\n", msgReceive)

			json.Unmarshal([]byte(msgReceive), &wattSenseReceive)
			number, err := strconv.Atoi(wattSenseReceive.Value)
			if err != nil {
				log.Printf("error ro convert int")
			}
			t := time.Now()
			wattSensePublish := []WattSensePublish{{PropertyId: wattSenseReceive.PropertyId, Timestamp: t, Value: number}}
			msgToPublish, err := json.Marshal(wattSensePublish)
			if err != nil {
				log.Printf("ERROR TO CONVERT: %s\n", err)
			}
			log.Println("Message published", string(msgToPublish))
			token := c.Publish(topic_publish, 0, false, msgToPublish)
			token.Wait()
		})
		// the connection handler is called in a goroutine so blocking here would hot cause an issue. However as blocking
		// in other handlers does cause problems its best to just assume we should not block
		go func() {
			_ = t.Wait() // Can also use '<-t.Done()' in releases > 1.2.0
			if t.Error() != nil {
				log.Printf("ERROR SUBSCRIBING: %s\n", t.Error())
			} else {
				log.Println("subscribed to: ", topic_subscribe)
			}

		}()

	}
	opts.OnReconnecting = func(mqtt.Client, *mqtt.ClientOptions) {
		log.Println("attempting to reconnect")
	}

	c := mqtt.NewClient(opts)

	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	// Messages will be delivered asynchronously so we just need to wait for a signal to shutdown
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)
	signal.Notify(sig, syscall.SIGTERM)

	<-sig
	fmt.Println("signal caught - exiting")
	c.Disconnect(1000)
	fmt.Println("shutdown complete")
}
