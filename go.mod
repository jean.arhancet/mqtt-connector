module veolia.com/mqtt-connector

go 1.13

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5 // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
)
